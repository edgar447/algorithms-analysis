﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Algortimai_Lab1
{
    class HashTableFile : HashTableAbstract // Hashtable implementation in binary file
    {
        public FileStream fileStream;
        string FileName;

        public HashTableFile(string filename, int length)
        {
            
            if (File.Exists(filename))
                File.Delete(filename);
            FileName = filename;
            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filename,
               FileMode.Append)))
                {
                    Byte[] data = new Byte[40];
                    for (int j = 0; j < length; j++) // Creating file with a certain amount of empty space
                    {
                        writer.Write(data);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
        }

        // Getting and setting element by index
        // Each element has 40 bytes (20 chars, 2 bytes each)
        public override string this[int index]
        {
            get {
                string result = "";
                for (int i = 0; i < 20; i++) // Iteratting through each character
                {

                    Byte[] data = new Byte[2];
                    fileStream.Seek(40 * index + 2*i, SeekOrigin.Begin);
                    fileStream.Read(data, 0,2); // Reading char
                    bool empty = true;
                        if(data[0] > 0 || data[1] > 0)
                            empty = false;
                    if (!empty)
                    {
                        char temp = BitConverter.ToChar(data,0);
                        result += temp; // Appending each character
                    }
                }
                return result;
                }

            set
            {
                                length += 1;
                                Byte[] data = new Byte[40];

                                fileStream.Seek(40 * index, SeekOrigin.Begin);

                                int pointer = 0;

                                for(int i = 0; i < value.Length; i++)
                                {
                                    Byte[] temp = BitConverter.GetBytes(value[i]);
                                    for(int j = 0; j < 2; j++)
                                    {
                                        data[pointer * 2 + j] = temp[j]; // Writting each character to array
                                    }
                                    pointer++;
                                }
                                fileStream.Write(data, 0, 40);   // Writting array to file
            }
        }

    }
}
