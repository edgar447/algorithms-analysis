﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    class BucketArrayOP : BucketArrayAbstract // Bucket sort implementation using array in RAM
    {
        public override double this[int index]
        {
            get
            {
                return Data[index];
            }
        }

        // Creating array with random data
       public BucketArrayOP(int n, int seed)
        {
            array = new List<ArrayAbstract>();
            for(int i = 0; i < ArrayLength; i++)
            {
                array.Add(new ArrayOP());
            }

            Data = new List<double>();
                Random random = new Random(seed);

            for (int i = 0; i < n; i++)
            {
                Data.Add(random.NextDouble());
            }
            length = n;

        }

        public override void AppendBucket(ArrayAbstract array)
        {
                for (int i = 0; i < array.Length; i++)
                {
                Data.Add(array[i]);
                }
        }
  
        public override void Erase()
        {
            Data = new List<double>();
        }

        public override void SortAndRecieve(ArrayAbstract array)
        {
            Program.InsertionSort(array);
            AppendBucket(array);
        }
    
    }
}
