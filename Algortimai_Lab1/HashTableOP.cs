﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    class HashTableOP : HashTableAbstract // Hashtable implementation in RAM
    {
        string[] Table;


        public HashTableOP(int tableSize)
        {

            Table = new string[tableSize];
        }

        public override string this[int index] { get => Table[index]; set => Table[index] = value; }
    }
}
