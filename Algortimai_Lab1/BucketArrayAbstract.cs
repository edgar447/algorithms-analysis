﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    public abstract class BucketArrayAbstract // Abstract class for Bucket sort implementation using array in RAM and binary file
    {
        protected const int ArrayLength = 10;
        protected int length;
        public int Length { get { return length; } }
        public abstract double this[int index] { get; }
        public abstract void AppendBucket(ArrayAbstract array); // Append value from single buccket to array
        public abstract void Erase();
        public abstract void SortAndRecieve(ArrayAbstract array); //Sort single bucket and write the result to array
        protected List<double> Data;
        
        protected List<ArrayAbstract> array; // Bucket container

        public void Print(int n)
        {
            for (int i = 0; i < n; i++)
                Console.Write(" {0:F5} ", this[i]);
            Console.WriteLine();
        }

        public void Sort()
        {
            for (int i = 0; i < length; i++)
            {
                ToBucket(Data[i]);
            }
            Erase();
            for (int i = array.Count-1; i >=0; i--)
            {
                SortAndRecieve(array[i]);
            }

        }

        // Sorting values by buckets
        public void ToBucket(double value)
        {
            int index = (int)Math.Truncate(value * 10);
            array[index].Add(value); 

        }

    }
}
