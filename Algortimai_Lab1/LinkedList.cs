﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Algortimai_Lab1
{
    public class LinkedList : ListAbstract //Linked list in RAM
    {
        public Node HeadNode { get; set; }
        private Node Last { get; set; }
        private Node current { get; set; }
        private Node smallPointer;
        private Node bigPointer;

        public FileStream fileStream;

        public LinkedList()
        {
        }

        //Creating list and filling it with random data
        public LinkedList(int n, int seed)
        {
            length = n;
            Random rand = new Random(seed);
            HeadNode = new Node(rand.NextDouble());
            current = HeadNode;
            for (int i = 1; i < length; i++)
            {
                current.Next = new Node(rand.NextDouble());
                current = current.Next;
            }
            Last = current;

        }

        public override void Add(double value)
        {
            length++;
            if (Last == null)
            {
                HeadNode = current = Last = new Node(value);
            }
            else
            {
                Last.Next = new Node(value, null);
                Last = Last.Next;
            }
        }

        public override bool NotNull()
        {
            return current != null;
        }

        public override double Head()
        {
            current = HeadNode;
            return HeadNode.Data;
        }

        public override double Next()
        {
            current = current.Next;
            return current.Data;
        }

        public override void SetSmallPointer()
        {
            smallPointer = current;
        }

        public override void SetBigPointer()
        {
            bigPointer = current;
        }

        public override void Swap(double small, double big)
        {
            smallPointer.Data = big;
            bigPointer.Data = small;
        }

        public override void Erase()
        {
            length = 0;
            Last = current = HeadNode = null;
        }


    }

}
