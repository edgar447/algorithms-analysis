﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Algortimai_Lab1
{
    class BucketListFile : ListFile
    {
        const int AmountOfBuckets = 10;

        int currentNode;
        List<ListFile> list = new List<ListFile>();

        // Creating buckets and binary file with random data
        public BucketListFile(string filename, int n, int seed) : base(filename, n, seed)
        {
            for (int i = 0; i < AmountOfBuckets; i++)
            {
                list.Add(new ListFile("f" + i.ToString() + ".dat"));
            }

    } 
        public void Sort()
        {
            ToBuckets();

            Byte[] data = BitConverter.GetBytes(4);
            Filestream.Seek(0, SeekOrigin.Begin);
            Filestream.Write(data, 0, 4);
            currentNode = BitConverter.ToInt32(data, 0);

            Erase();
            for(int i = AmountOfBuckets-1; i >= 0; i--)
            {
                SortAndRecieveSingleBucket(list[i]);
            }
        }

        void SortAndRecieveSingleBucket(ListFile list)
        {
            if(list.Length > 0)
            using (list.Filestream = new FileStream(list.FileName, FileMode.Open, FileAccess.ReadWrite))
            {
                Program.InsertionSort(list);
                AppendBucket(list);

            }
        }

        // Appending single bucket to list
        void AppendBucket(ListFile bucket)
        {
                double value = bucket.Head();
                for (int i = 0; i < bucket.Length; i++)
                {
                    Filestream.Seek(currentNode, SeekOrigin.Begin);
                    Byte[] data = BitConverter.GetBytes(value);
                    Filestream.Write(data, 0, 8); // Writing value
                Filestream.Seek(currentNode + 8, SeekOrigin.Begin);
                currentNode += 12; // Incrementing pointer 
                    data = BitConverter.GetBytes(currentNode);

                Filestream.Write(data, 0, 4); // Writting pointer

                    if (i < bucket.Length - 1)
                        value = bucket.Next();
                }
        }

        // Sorting values by buckets
        public void ToBuckets()
        {
            double value = Head();
            for (int i = 0; i < length; i++)
            {
                int index = (int)Math.Truncate(value * 10);
                list[index].Add(value);
                if (i < length - 1)
                    value = Next();
            }
        }
    }
}
