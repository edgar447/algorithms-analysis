﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Algortimai_Lab1
{
    class ListFile : ListAbstract //Linked list in binary file
    {
        public string FileName { get; set; }
        int currentNode;
        int nextNode; // Value of next node of currentNode
        int last;

        int minValuePointer;
        int maxValuePointer;
        
        // Creating empty list
        public ListFile(string filename)
        {

            FileName = filename;
            if (File.Exists(filename))
                File.Delete(filename);
            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(FileName,
               FileMode.Create)))
                {

                    currentNode = 4;
                    Byte[] data = BitConverter.GetBytes(currentNode);
                    writer.Write(data);
                    last = currentNode;
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        // Creating empty list and filling it with random data
        public ListFile(string filename, int n, int seed)
        {
            length = n;
            Random random = new Random(seed);
            FileName = filename;

            if (File.Exists(filename)) File.Delete(filename);
            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filename,
               FileMode.Create)))
                {  // First 8 bytes are used to save value, 4 for a pointer (12 total)
                    writer.Write(4); // Pointer to the first element
                    for (int j = 0; j < length; j++)
                    {
                        writer.Write(random.NextDouble()); // Value
                        writer.Write((j + 1) * 12 + 4); // Pointer to the next element 
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        public FileStream Filestream { get; set; }

        // Adding value to the end of file
        public override void Add(double value)
        {
            length += 1;
            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(FileName,
               FileMode.Append)))
                {          
                        Byte[] data = BitConverter.GetBytes(value);
                        writer.Write(value);
                    last += 12; // Incrementing pointer to the last element, each node has 12 bytes
                    writer.Write(last);

                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        // Cheking if current node is not null
        public override bool NotNull()
        {
            Byte[] data = new Byte[12];
            Filestream.Seek(currentNode, SeekOrigin.Begin);
            bool notNull = false;
            Filestream.Read(data, 0, 12);

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] > 0)
                {                    
                    notNull = true;
                    break;
                }
            }
            return notNull;
        }

        public override double Head()
        {
            Byte[] data = new Byte[12];
            Filestream.Seek(0, SeekOrigin.Begin);
            Filestream.Read(data, 0, 4); //Reading a pointer to the Head element
            currentNode = BitConverter.ToInt32(data, 0); // Current node is set to Head element
            Filestream.Seek(currentNode, SeekOrigin.Begin);
            Filestream.Read(data, 0, 12);
            double result = BitConverter.ToDouble(data, 0);
            nextNode = BitConverter.ToInt32(data, 8); // Saving value of second element
            return result;
        }

        public override double Next()
        {
            Byte[] data = new Byte[12];
            Filestream.Seek(nextNode, SeekOrigin.Begin);

            Filestream.Read(data, 0, 12);
            currentNode = nextNode;
            double result = BitConverter.ToDouble(data, 0);
            nextNode = BitConverter.ToInt32(data, 8);
            return result;
        }

        public override void Swap(double small, double big)
        {
            Byte[] data;
            Filestream.Seek(minValuePointer, SeekOrigin.Begin);
            data = BitConverter.GetBytes(big);
            Filestream.Write(data, 0, 8);
            Filestream.Seek(maxValuePointer, SeekOrigin.Begin);
            data = BitConverter.GetBytes(small);
            Filestream.Write(data, 0, 8);

        }

        public override void SetSmallPointer()
        {
            minValuePointer = currentNode;
        }

        public override void SetBigPointer()
        {
            maxValuePointer = currentNode;
        }

        public override void Erase()
        {
            length = 0;
            currentNode = last = 4;

        }


    }
}
