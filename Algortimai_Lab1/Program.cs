﻿//---------TASK------------
// Make an analysis of Bucket and Insertion sort implemented in array(or list) and Linked list and Hash table search.
// In all cases analyse performance when sorting or searching is done in Operative memory and binary file.
// First make sure that each algorithm works correctlly, then make an analysis.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Algortimai_Lab1
{
    class Program
    {

        static Random random = new Random();  // Used to generate random string for a Hash table
        static int count; // Used to control length of randomly generated strings

        static void Main(string[] args)
        {
            count = 0;
            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;

            OPInsertion(seed);     //--------------------

            ToFileInsertion(seed); //  Checking that algrithms work correctlly, in each case, both array and Linked list is checked

            OPBucket(seed);        // 

            ToFileBucket(seed);    //--------------------

            HashTableTest();

            Analysis();

            Console.ReadLine();
        }
        
        public static void Analysis()
        {
            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;
            int[] amount = { 100, 200, 400, 800, 1600, 3200, 6400 };
            Console.WriteLine("Hash Table");
            Console.WriteLine();
            Console.WriteLine("{0,8} {1,8} {2,8}", "n", "File", "OP");
            foreach (int n in amount)
            {
                Console.WriteLine();

                Stopwatch stop2 = new Stopwatch();

                HashTableOP dataArray = new HashTableOP(10000);
                for (int i = 0; i < n / 10; i++)
                {
                    string temp1 = RandomString(6, seed);
                    string temp2 = RandomString(6, seed);
                    dataArray.Add(temp1, temp2);
                }
                dataArray.Add("Edgar", "Reis");
                stop2.Start();
                dataArray.Exist("Edgar", "Reis");
                stop2.Stop();

                string filename = @"mydatalist.dat";
                HashTableFile table1 = new HashTableFile(filename, 10000);

                Stopwatch stop1 = new Stopwatch();

                using (table1.fileStream = new FileStream(filename, FileMode.Open,
               FileAccess.ReadWrite))
                {
                    for (int i = 0; i < n / 10; i++)
                    {
                        string temp1 = RandomString(6, seed);
                        string temp2 = RandomString(6, seed);
                    }
                    table1.Add("Edgar", "Reis");


                    stop1.Start();
                    table1.Exist("Edgar", "Reis");
                    stop1.Stop();
                }
                Console.WriteLine("{0,8}  {1,8}  {2,8}", n, stop1.ElapsedMilliseconds, stop2.ElapsedMilliseconds);


            }


            Console.WriteLine("Insertion sort array");
            Console.WriteLine();
            Console.WriteLine("{0,8} {1,8} {2,8}", "n", "File", "OP");
            foreach (int n in amount)
            {
                Console.WriteLine();
                string filename = @"mydatalist.dat";
                ArrayFile dataToFile = new ArrayFile(filename, n, seed);
                Stopwatch stop1 = new Stopwatch();
                using (dataToFile.fileStream = new FileStream(filename, FileMode.Open,
               FileAccess.ReadWrite))
                {
                    stop1.Start();
                    InsertionSort(dataToFile);
                    stop1.Stop();
                }
                Stopwatch stop2 = new Stopwatch();

                ArrayOP dataArray = new ArrayOP(n, seed);
                stop2.Start();
                InsertionSort(dataArray);
                stop2.Stop();
                Console.WriteLine("{0,8}  {1,8}  {2,8}", n, stop1.ElapsedMilliseconds, stop2.ElapsedMilliseconds);

            }
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Insertion sort Linked list");
            Console.WriteLine();
            Console.WriteLine("{0,8} {1,8} {2,8}", "n", "File", "OP");
            foreach (int n in amount)
            {
                Console.WriteLine();
                string filename = @"mydatalist.dat";
                ListFile dataToFile = new ListFile(filename, n, seed);
                Stopwatch stop1 = new Stopwatch();
                using (dataToFile.Filestream = new FileStream(filename, FileMode.Open,
               FileAccess.ReadWrite))
                {
                    stop1.Start();
                    InsertionSort(dataToFile);
                    stop1.Stop();
                }
                Stopwatch stop2 = new Stopwatch();

                LinkedList dataArray = new LinkedList(n, seed);
                stop2.Start();
                InsertionSort(dataArray);
                stop2.Stop();
                Console.WriteLine("{0,8}  {1,8}  {2,8}", n, stop1.ElapsedMilliseconds, stop2.ElapsedMilliseconds);

            }
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Bucket sort array");
            Console.WriteLine();
            Console.WriteLine("{0,8} {1,8} {2,8}", "n", "File", "OP");
            foreach (int n in amount)
            {
                Console.WriteLine();
                string filename = @"mydatalist.dat";
                BucketArrayFile dataToFile = new BucketArrayFile(filename, n, seed);
                Stopwatch stop1 = new Stopwatch();
                using (dataToFile.Filestream = new FileStream(filename, FileMode.Open,
               FileAccess.ReadWrite))
                {
                    stop1.Start();
                    dataToFile.Sort();
                    stop1.Stop();
                }
                Stopwatch stop2 = new Stopwatch();

                BucketArrayOP dataArray = new BucketArrayOP(n, seed);
                stop2.Start();
                dataArray.Sort();
                stop2.Stop();
                Console.WriteLine("{0,8}  {1,8}  {2,8}", n, stop1.ElapsedMilliseconds, stop2.ElapsedMilliseconds);
            }

            Console.WriteLine("Bucket sort Linked list");
            Console.WriteLine();
            Console.WriteLine("{0,8} {1,8} {2,8}", "n", "File", "OP");
            foreach (int n in amount)
            {
                Console.WriteLine();
                string filename = @"mydatalist.dat";
                BucketListFile dataToFile = new BucketListFile(filename, n, seed);
                Stopwatch stop1 = new Stopwatch();
                using (dataToFile.Filestream = new FileStream(filename, FileMode.Open,
               FileAccess.ReadWrite))
                {
                    stop1.Start();
                    dataToFile.Sort();
                    stop1.Stop();
                }
                Stopwatch stop2 = new Stopwatch();

                BucketListOP dataArray = new BucketListOP(n, seed);
                stop2.Start();
                dataArray.Sort();
                stop2.Stop();
                Console.WriteLine("{0,8}  {1,8}  {2,8}", n, stop1.ElapsedMilliseconds, stop2.ElapsedMilliseconds);
            }


        }

        public static string RandomString(int length, int seed)
        {                   

          string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int ln = 0;
            
            if (count < 1) // Each generated string varies in length
                ln = 3;
            else if (count < 2)
                ln = 4;
            else if (count < 3)
                ln = 5;
            else if (count < 4)
                ln = 6;
            else if (count < 5)
                ln = 7;
            count++;
            if (count > 4)
                count = 1;

            string value = new string (Enumerable.Repeat(chars, ln)  
              .Select(s => s[random.Next(s.Length)]).ToArray());
        return value;
        }

        // Implemented hash table doesn't change it's size when filled more than half
        public static void HashTableTest()
        {
            Console.WriteLine("Hash table OP test");
            HashTableOP table = new HashTableOP(10000);
            Console.WriteLine("Hash table");
            table.Add("Edgar", "Reis");
            table.Add("Edgar", "Reis");    // Check up consists only from adding same element twice and waiting for an error message to appear on the screen
            Console.WriteLine();           

            HashTableFile table2 = new HashTableFile("w.dat", 10000);
            Console.WriteLine("Hash table in file test");

            using (table2.fileStream = new FileStream("w.dat", FileMode.Open,
           FileAccess.ReadWrite))
            {

                table2.Add("Edgar", "Reis");
                table2.Add("Edgar", "Reis");

            }
        }

        public static void ToFileBucket(int seed)
        {
            Console.WriteLine();
            Console.WriteLine("Bucket sort in file"); 
            Console.WriteLine();
            Console.WriteLine();
            int n = 12;
            string filename;

            filename = @"mydatalist.dat";
            BucketArrayFile dataToFile = new BucketArrayFile(filename, n, seed);
            using (dataToFile.Filestream = new FileStream(filename, FileMode.Open,
           FileAccess.ReadWrite))
            {
                Console.WriteLine("\n ARRAY \n");
                dataToFile.Print(n);
                dataToFile.Sort();
                dataToFile.Print(n);
            }

                filename = @"daa.dat";
                BucketListFile data = new BucketListFile(filename, n, seed);
                using (data.Filestream = new FileStream(filename, FileMode.Open,
               FileAccess.ReadWrite))
                {
                    Console.WriteLine("\n List \n");
                    data.Print(n);
                    data.Sort();
                    data.Print(n);
                }            
        }  //---------------------------------------------
                                                      //
        public static void OPBucket(int seed)
        {
            Console.WriteLine();
            Console.WriteLine("Bucket sort in OP");
            Console.WriteLine();

            int n = 12;

            BucketArrayOP dataToFile = new BucketArrayOP(n, seed);
                Console.WriteLine("\n ARRAY \n");
                dataToFile.Print(n);
                dataToFile.Sort();
                dataToFile.Print(n);

            BucketListOP data = new BucketListOP(n, seed);
            Console.WriteLine("\n List \n");
            data.Print(n);
            data.Sort();
            data.Print(n);

        }      //
                                                      // Initializing data structeres and printing them before and after sort
        public static void ToFileInsertion(int seed)
        {
            Console.WriteLine();
            Console.WriteLine("Insertion sort in file");
            Console.WriteLine();

            int n = 12;
            string filename;

            filename = @"mydataarray.dat";
            ListFile myfilearray = new ListFile(filename, n, seed);
            using (myfilearray.Filestream = new FileStream(filename, FileMode.Open,
           FileAccess.ReadWrite))
            {
                Console.WriteLine("\n LIST \n");
                myfilearray.Print(n);
                InsertionSort(myfilearray);
                myfilearray.Print(n);
            }


            filename = @"mydatalist.dat";
            ArrayFile dataToFile = new ArrayFile(filename, n, seed);
            using (dataToFile.fileStream = new FileStream(filename, FileMode.Open,
           FileAccess.ReadWrite))
            {
                Console.WriteLine("\n ARRAY \n");
                dataToFile.Print(n);
                InsertionSort(dataToFile);
                dataToFile.Print(n);
            }
        }
                                                      //
        public static void OPInsertion(int seed)
        {
            Console.WriteLine();
            Console.WriteLine("Insertion sort in OP");
            Console.WriteLine();

            int n = 12;

            ArrayOP dataArray = new ArrayOP(n, seed);

                Console.WriteLine("\n OP ARRAY \n");
            dataArray.Print(n);
                InsertionSort(dataArray);
            dataArray.Print(n);

            LinkedList dataList = new LinkedList(n, seed);

            Console.WriteLine("\n OP LIST \n");
            dataList.Print(n);
            InsertionSort(dataList);
            dataList.Print(n);
        }   //----------------------------------------------

        public static void InsertionSort(ListAbstract list)
        {
            double max = 0;
            bool change;
            double current = list.Head();
            list.SetSmallPointer();
            for (int i = 0; i < list.Length; i++)
            {

                current = list.Head();
                for (int h = 0; h < i; h++)
                {
                    current = list.Next();
                }
                list.SetSmallPointer();

                change = false;
                max = current;
                double startValue = current;
                for (int j = i + 1; j < list.Length; j++)
                {
                    current = list.Next();
                    if (current > max)
                    {
                        change = true;
                        max = current;
                        list.SetBigPointer();
                    }
                }
                if (change)
                    list.Swap(startValue, max);


            }
        }
 
        public static void InsertionSort(ArrayAbstract Data)
        {
            for (int i = 0; i < Data.Length - 1; i++)
            {
                double max = Data[i];
                int index = 0;
                bool swap = false;
                for (int j = i + 1; j < Data.Length; j++)                   
                {
                    if (max < Data[j])
                    {
                        max = Data[j];
                        index = j;
                        swap = true;
                    }
                }
                if (swap)
                {
                    Data.Swap(i, index, Data[i], max);
                }
            }
        }

    }
}
