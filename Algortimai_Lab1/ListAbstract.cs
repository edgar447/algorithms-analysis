﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    public abstract class ListAbstract // Abstract class for Insertion sort implementation using Linked list in RAM and binary file
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract double Head();
        public abstract double Next();
        public abstract bool NotNull();
        public abstract void SetSmallPointer();  // Saving a pointer of smaller value for swaping
        public abstract void SetBigPointer();    // Saving a pointer of bigger value, for swaping
        public abstract void Add(double value);
        public abstract void Erase();
        public abstract void Swap(double small, double big);
        public void Print(int n)
        {

            Console.Write(" {0:F5} ", Head());
            for (int i = 1; i < n; i++)
                Console.Write(" {0:F5} ", Next());
            Console.WriteLine();

        }

    }
}
