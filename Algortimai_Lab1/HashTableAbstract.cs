﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
   public abstract class HashTableAbstract // Implemented Hash table doens't change it's size when filled up to half. 
    {                                      // For searching in operative memory and file, there is separate 
        protected int length;
        public int Length { get { return length; } }
        public abstract string this[int index] { get; set; } // Allows usage of same methods for both RAM and binary file implementations


        // Checking if item exists in a table
        public bool Exist(string name, string lastName)
        {
            // Hash code is calculated from both name and lastname
            string value = name.First().ToString().ToUpper() + name.Substring(1).ToLower() + lastName.First().ToString().ToUpper() + lastName.Substring(1).ToLower();

            int hash = PrimaryHash(value);
            if (this[hash] == null || this[hash] == "")
                return false;

            if (this[hash].Equals(value))
                return true;
            else
            {
                int shift = 0;
                while (true)  // Loop will stops working when program will reach end of hash table or find an empty spot, or find desired value.
                {
                    shift++;
                    hash = SecondaryHash(value, shift);
                    if (hash == Length || this[hash] == null)
                        return false;

                    if (this[hash].Equals(value))
                        return true;

                }
            }

        }

        public void Add(string name, string lastName)
        {
            // Hash code is calculated from both name and lastname
            string value = name.First().ToString().ToUpper() + name.Substring(1).ToLower() + lastName.First().ToString().ToUpper() + lastName.Substring(1).ToLower();
            length++;
            int hash = PrimaryHash(value);

            if (this[hash] == null || this[hash] == "")
                this[hash] = value;
            else if (Exist(name, lastName))
            {
                Console.WriteLine("Error. Trying to add existing Key.");
                return;
            }
            else
            {
                bool notInserted = true;
                int shift = 0;
                while (notInserted) // Loop will stops working when program will reach end of hash table or find an empty spot
                {

                    shift++;
                    hash = SecondaryHash(value, shift);

                    if (hash == Length)
                    {
                        Console.WriteLine("-----FATAL ERROR!------");
                        Console.WriteLine("Collision resolving failed. Value cannot be inserted");
                        return;
                    }
                    if (this[hash] == null)
                    {
                        this[hash] = value;
                        notInserted = false;
                    }
                }
            }

        }

        static int PrimaryHash(string value)
        {
            int hash = 0;

            for (int i = 0; i < value.Length; i++)
            {
                hash += (value[i] - 64) * (i + 1);
            }
            return hash;
        }

        static int SecondaryHash(string value, int shift)
        {
            int hash = 0;

            for (int i = 0; i < value.Length; i++)
            {
                hash += (value[i] - 64) * (i + 1);
            }
            return hash += shift;
        }

    }
}
