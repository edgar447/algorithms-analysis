﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    public abstract class ArrayAbstract  // Abstract class for Insertion sort implementation using array in RAM and binary file
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract double this[int index] { get; } // Allows use of same printing method for both standart array and array in a binary file
        public abstract void Add(double value);
        public abstract void Swap(int posSmall, int posBig, double small, double big);
        public void Print(int n)
        {
            for (int i = 0; i < n; i++)
                Console.Write(" {0:F5} ", this[i]);
            Console.WriteLine();
        }
    }
}
