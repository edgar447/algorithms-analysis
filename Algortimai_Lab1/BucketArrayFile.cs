﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Algortimai_Lab1
{
    public class BucketArrayFile : BucketArrayAbstract
    {
        public FileStream Filestream { get; set; }

        int pointer;

        string filename;
        
        public BucketArrayFile(string filename, int length, int seed)
        {
            array = new List<ArrayAbstract>();

            this.filename = filename;

            if (File.Exists(filename))
                File.Delete(filename);
            for (int i = 0; i < ArrayLength; i++)
            {
                array.Add(new ArrayFile("file" + i.ToString() + ".dat"));
            }

            Data = new List<double>();
            Random random = new Random(seed);
            for (int i = 0; i < length; i++)
            {
                Data.Add(random.NextDouble());
            }

            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filename,
               FileMode.Create)))
                {
                    for (int j = 0; j < length; j++)
                        writer.Write(Data[j]);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }

            this.length = length;

        }



        public override void SortAndRecieve(ArrayAbstract array)
        {
            if (array.Length > 0)
            {
                ArrayFile newArray = array as ArrayFile;
                using (newArray.fileStream = new FileStream(newArray.FileName, FileMode.Open, FileAccess.ReadWrite))
                {
                    Program.InsertionSort(newArray);
                    AppendBucket(newArray);
                }
            }
        }

        public override double this[int index]
        {
            get
            {
                Byte[] data = new Byte[8]; // Each value has 8 bytes
                Filestream.Seek(8 * index, SeekOrigin.Begin);
                Filestream.Read(data, 0, 8);
                double result = BitConverter.ToDouble(data, 0);
                return result;
            }
        }

        public override void AppendBucket(ArrayAbstract array)
        {
            try
            {
                for (int i = 0; i < array.Length; i++)
                {
                    Byte[] data = BitConverter.GetBytes(array[i]); // Converting value to bytes
                    Filestream.Seek(pointer, SeekOrigin.Begin);
                    Filestream.Write(data, 0, 8);
                    pointer += 8; // Each value has 8 bytes

                }

            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }


        }

        public override void Erase()
        {
        }
       
    }
}
