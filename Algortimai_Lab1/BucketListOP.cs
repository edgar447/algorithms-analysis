﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    class BucketListOP : LinkedList // Bucket sort implementation using linked list in Opeartive memory
   {
        const int AmountOfBuckets = 10; 

        List<LinkedList> list = new List<LinkedList>();

        // Creating buckets and list filled with random data
        public BucketListOP(int n, int seed) : base(n, seed)
        {
            for(int i = 0; i < AmountOfBuckets; i++)
            {
                list.Add(new LinkedList());
            }
        }

        public void Sort()
        {
            ToBuckets();

            Erase();

            for (int i = AmountOfBuckets - 1; i >= 0; i--)
            {
                SortAndRecieveSingleBucket(list[i]);
            }

        }

        void SortAndRecieveSingleBucket(LinkedList list)
        {
            if (list.Length > 0)
            {
                Program.InsertionSort(list);
                GetFromSingleBucket(list);
            }
        }

        void GetFromSingleBucket(LinkedList bucket)
        {
            double value = bucket.Head();
            for (int i = 0; i < bucket.Length; i++)
            {
                Add(value);

                if (i < bucket.Length - 1)
                    value = bucket.Next();
            }


        }

        public void ToBuckets()
        {
            double value = Head();

            for (int i = 0; i < length; i++)
            {
                int index = (int)Math.Truncate(value * 10);
                list[index].Add(value);

                if (i < length - 1)
                    value = Next();
            }
        }
    }

}
