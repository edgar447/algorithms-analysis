﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Algortimai_Lab1
{
    public class ArrayFile : ArrayAbstract  // Array in a binary file
    {
        public FileStream fileStream { get; set; }

        public string FileName { get; private set; }

        public override void Add(double value)
        {
            length += 1;
            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(FileName,
               FileMode.Append)))
                {
                    writer.Write(value);

                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //Creating empty array
        public ArrayFile(string filename)
        {
            FileName = filename;
            if (File.Exists(filename))
                File.Delete(filename);

        }

        // Creating array and filling it with random data
        public ArrayFile(string filename, int length, int seed)
        {
            FileName = filename;
            double[] data = new double[length];
            Random random = new Random(seed);
            for (int i = 0; i < length; i++)
            {
                data[i] = random.NextDouble(); //Generating random values
            }

            this.length = length;

            if (File.Exists(filename))
                File.Delete(filename);

            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filename,
               FileMode.Create)))
                {
                    for (int j = 0; j < length; j++)
                        writer.Write(data[j]); //Writing values to file
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
   
        public override double this[int index]
        {
            get // Returns element by index
            {
                Byte[] data = new Byte[8];
                fileStream.Seek(8 * index, SeekOrigin.Begin);
                fileStream.Read(data, 0, 8);
                double result = BitConverter.ToDouble(data, 0);
                return result;

            }
        }

        public override void Swap(int posSmall, int posBig, double small, double big)
        {
            Byte[] Small = new Byte[8];
            Byte[] Big = new Byte[8];
            BitConverter.GetBytes(small).CopyTo(Small, 0);
            BitConverter.GetBytes(big).CopyTo(Big, 0);

            fileStream.Seek(posSmall * 8, SeekOrigin.Begin);
            fileStream.Write(Big, 0, 8);
            fileStream.Seek(posBig * 8, SeekOrigin.Begin);
            fileStream.Write(Small, 0, 8);
        }
    }

}
