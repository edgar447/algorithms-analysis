﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    class ArrayOP : ArrayAbstract // Array in operative memory
    {
        List<double> data;

        public ArrayOP(int n, int seed)
        {
            data = new List<double>();
            length = n;
            Random rand = new Random(seed);

            for (int i = 0; i < length; i++)
            {
                data.Add(rand.NextDouble());
            }
        }
         
        public ArrayOP()
        {
            data = new List<double>();
        }

        public override double this [int index]
        {
            get { return data[index]; }
            
        }
        
       public override void Add(double value)
        {
            data.Add(value);
            length++;
        }

        public override void Swap(int posSmall, int posBig, double small, double big)
        {
            data[posSmall] = big;
            data[posBig] = small;
        }
    }

}
