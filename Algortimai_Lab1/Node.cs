﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algortimai_Lab1
{
    public class Node  // Node of a Linked list
    {
        public double Data { get; set; }
        public Node Next { get; set; }

        public Node(double data, Node next)
        {
            Data = data;
            Next = next;
        }

        public Node(Node next)
        {
            Next = next;
        }

        public Node(double data)
        {
            Data = data;
            Next = null;
        }

        public Node()
        { }
    }
}
